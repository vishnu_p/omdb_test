
import UIKit

class MovieCell: UICollectionViewCell {
    
    
    @IBOutlet weak var movieImage: YUCustomImageView!
    @IBOutlet weak var movieName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
       // movieImage.image = UIImage(named: "placeholder_for_missing_posters")
        movieName.text = ""
    }
}
