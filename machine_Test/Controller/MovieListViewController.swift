
import UIKit


class MoviewListViewController: UIViewController {

    //Movie list VC outlets
    @IBOutlet weak var collectinView: UICollectionView!
    var ListViewModel = MovieListViewModel()
    //Custom properties
    var isLoadingList = false
    fileprivate var movieDetails : [MoviePageDetails]?
    fileprivate var pageNumber = 1
    fileprivate var totalContent = 0
    fileprivate var pageTitle = ""
    
    struct movieCellData {
           
           var flag = String()
           var displayName = String()
        
       }
    var tableSearchData = [movieCellData]()
    var tableViewData = [movieCellData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpUI()
        self.fetchMovieDetails(pageNumber: pageNumber)
    }
    
    //Setup UI
    fileprivate func setUpUI() {
        //Register collectionview cell from nib
        let nib = UINib(nibName: "MovieCell", bundle: nil)
        collectinView.register(nib, forCellWithReuseIdentifier: "MovieCell")
    }
    
    //Fetch movie details
    fileprivate func fetchMovieDetails(pageNumber : Int){
        ListViewModel.callListView(pageNum: pageNumber) { outPutList,listCount  in
            print(outPutList)
            self.isLoadingList = false
            let intCount = Int(listCount)
            self.totalContent = intCount ?? 0
            for data in outPutList{
                let test = movieCellData(flag : data.poster ?? "",displayName: data.title ?? "")
                self.tableViewData.append(test)
            }
            
            self.tableSearchData = self.tableViewData
            
            DispatchQueue.main.async {
                self.collectinView.reloadData()
                
            }
            
        }
    }
    
}

//MARK: Collection view delegate & datasource
extension MoviewListViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tableViewData.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
        cell.movieName.text = tableViewData[indexPath.row].displayName
       
       // print(movieDetails![indexPath.item].posterImage)
//        if let missingData = tableViewData[indexPath.row].flag{
//            if  missingData == "posterthatismissing.jpg"{
//                cell.movieImage.image = UIImage(named: "image-gallery.png")
//            }
//            else{
               // movieImage
        cell.movieImage.loadImage(from: tableViewData[indexPath.row].flag, defaultImageName: "image-gallery.png")
                //cell.movieImage.image = UIImage(named: movieDetails?[indexPath.item].poster ?? "placeholder_for_missing_posters")
           // }
        //}
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        let delH = Double(size) * 0.5
        return CGSize(width: size, height: size+Int(delH))
        
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                print("...",pageNumber,"...")
                pageNumber += 1
                fetchMovieDetails(pageNumber: pageNumber)
               // self.isLoadingList = true
            }
        
        
        }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard searchText != "" else {
            
            tableViewData = tableSearchData
            self.collectinView.reloadData()
            return
        }

        tableViewData = tableSearchData.filter { $0.displayName.contains(searchText) }

       DispatchQueue.main.async {
           
        self.collectinView.reloadData()
           
       }

}

    
}

    
    
    

