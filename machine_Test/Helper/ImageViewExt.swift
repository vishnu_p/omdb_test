import UIKit
import Alamofire


let imageCache = NSCache<NSString,AnyObject>()


class YUCustomImageView : UIImageView {
    var loadingIndicator = UIActivityIndicatorView(style: .gray)
    var downloadTask : DataRequest?
    
    func loadImage(from imageUrlString : String, defaultImageName : String?) {
        
        if imageUrlString == "" {
            
//            let avatarImage = LetterAvatarMaker()
//                .setUsername(defaultImageName ?? "")
//            .setBackgroundColors([ #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1) ])
//                .setLettersColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2086817782))
//            .build()
//            self.image = avatarImage
            return
        }
        
        self.image = nil
        
        if let downloadTask = downloadTask {
            print("Download task cancelled")
            downloadTask.cancel()
        }
        
        if let data = imageCache.object(forKey: imageUrlString as NSString) as? Data {
            if let image = UIImage(data: data) {
                self.image = image
                return
            }
        }
        
        if let url = URL(string: imageUrlString) {
            loadSpinner()
            downloadTask = AF.request(url, method: .get,encoding: JSONEncoding.default, headers: nil).responseData
                {response in
                    switch response.result {
                    case .success:
                        
                        if let responseData = response.data{
                            self.removeSpinner()
                            
                            if let image = UIImage(data: responseData) {
                                imageCache.setObject(response.data! as AnyObject, forKey: imageUrlString as NSString)
                                
                                self.image = image
                            }
                        }
                        break
                    case .failure(let error):
                        
                        print(error)
                        break
                    }
                    
            }
            downloadTask?.responseData { (response) in
                DispatchQueue.main.async {
                    self.removeSpinner()
                    if response.error == nil && response.data != nil  {
                        if let image = UIImage(data: response.data!) {
                            imageCache.setObject(response.data! as AnyObject, forKey: imageUrlString as NSString)
                            self.image = image
                        }
                    }
                }
            }.resume()
            
        }
    }
    
    func loadSpinner()  {
        self.addSubview(loadingIndicator)
        loadingIndicator.color = UIColor.blue
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loadingIndicator.startAnimating()
    }
    
    func removeSpinner()  {
        loadingIndicator.removeFromSuperview()
    }
}
