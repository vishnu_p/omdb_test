//
//  MovieList.swift
//

import Foundation

struct MoviesPage : Codable {
    
      let totalResults : String?
      let Response : String?
    
      let Search : [MoviePageDetails]?
}

struct MoviePageDetails : Codable {
    
    let title : String?
    let year : String?
    let imdbID : String?
    let type : String?
    let poster : String?
    
    enum CodingKeys : String,CodingKey
    {
        case title = "Title"
        case year = "Year"
        case imdbID
        case type = "Type"
        case poster = "Poster"
    }
}
